package com.instaprep.board

data class BoardItemData(val id: Int? = 0, val boardName: String? = "", var selected:Boolean)