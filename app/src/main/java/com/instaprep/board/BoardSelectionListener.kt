package com.instaprep.board

interface BoardSelectionListener {

    fun selectedBoard(item:BoardItemData)
}