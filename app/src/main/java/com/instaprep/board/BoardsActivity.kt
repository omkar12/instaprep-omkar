package com.instaprep.board

import android.animation.ObjectAnimator
import android.os.Handler
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.instaprep.R
import com.instaprep.classes.ClassesActivity
import com.instaprep.databinding.ActivityBoardsBinding
import com.instaprep.utils.StaticKeysCode
import com.instaprep.utils.snackBar
import com.dorna.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_boards.*

class BoardsActivity : BaseActivity(), BoardSelectionListener {

    private lateinit var activityBoardsBinding: ActivityBoardsBinding
    private var dataList: ArrayList<BoardItemData> = ArrayList()
    private lateinit var adapter: BoardsAdapter
    private var selectedBoard: Boolean = false

    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 1000 //1 seconds
    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            moveToNext(ClassesActivity::class.java, finishCurrent = false, clearStack = false)
        }
    }

    override fun initView() {
        activityBoardsBinding =
            DataBindingUtil.setContentView(this@BoardsActivity, R.layout.activity_boards)
    }

    override fun initData() {
        adapter = BoardsAdapter(context = this, boardList = dataList, itemClickListener = this)
        boardRecyclerView.layoutManager = LinearLayoutManager(this@BoardsActivity)
        boardRecyclerView.hasFixedSize()
        boardRecyclerView.adapter = adapter

        mDelayHandler = Handler()

        addTempData()
    }

    private fun addTempData() {
        if (dataList.isNotEmpty()) dataList.clear()

        dataList.add(BoardItemData(1, "HSC", false))
        dataList.add(BoardItemData(2, "CBSE", false))
        dataList.add(BoardItemData(3, "ICSE", false))
        dataList.add(BoardItemData(4, "IGSE", false))
        adapter.notifyDataSetChanged()
    }

    override fun initListener() {
        submitButton.setOnClickListener {
            if (selectedBoard) {
                moveToNext(ClassesActivity::class.java, finishCurrent = true, clearStack = true)
                overridePendingTransition(R.anim.enter, R.anim.exit)
            } else rootView.snackBar(getString(R.string.warning_select_board_first))
            //   mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)
        }
    }

    override fun selectedBoard(item: BoardItemData) {
        if (!selectedBoard) {
            selectedBoard = true
            showContinue()
        }
    }

    private fun showContinue() {
        submitButton.visibility = View.VISIBLE
        ObjectAnimator.ofFloat(
            submitButton, "translationY", StaticKeysCode.SCREEN_HEIGHT.toFloat(), 0f
        ).apply {
            duration = 1000
            start()
        }
    }

    override fun onDestroy() {
        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }
        super.onDestroy()
    }
}