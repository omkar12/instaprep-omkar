package com.instaprep.board

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.instaprep.R
import kotlinx.android.synthetic.main.activity_board_item.view.*

class BoardsAdapter : RecyclerView.Adapter<BoardsAdapter.BoardItemViewHolder> {

    private val context: Context
    private val boardList: ArrayList<BoardItemData>
    private val itemClickListener: BoardSelectionListener
    private var lastSelectedPosition = 0

    constructor(
        context: Context,
        boardList: ArrayList<BoardItemData>,
        itemClickListener: BoardSelectionListener
    ) : super() {
        this.context = context
        this.boardList = boardList
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoardItemViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.activity_board_item, parent, false)
        return BoardItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return boardList.size
    }

    override fun onBindViewHolder(holder: BoardItemViewHolder, position: Int) {
        holder.bindItem(context, boardList[position])

        holder.itemView.setOnClickListener {
            boardList[position].selected = true
            notifyItemChanged(position)
            if (lastSelectedPosition !=position){
                boardList[lastSelectedPosition].selected = false
                notifyItemChanged(lastSelectedPosition)
                lastSelectedPosition = position
            }
            itemClickListener.selectedBoard(boardList[position])
        }
    }

    class BoardItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(context: Context, boardItemData: BoardItemData) {
            itemView.boardNameText.text = boardItemData.boardName
            if (boardItemData.selected)
                itemView.cardView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBgBoardSelected))
            else itemView.cardView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite))
        }

    }
}