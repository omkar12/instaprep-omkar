package com.instaprep.welcome

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.instaprep.R
import com.instaprep.databinding.FragmentWelcomeTwoBinding
import com.instaprep.utils.StaticKeysCode
import com.dorna.ui.base.BaseFragment

class WelcomeTwoFragment : BaseFragment() {

    private lateinit var fragmentWelcomeTwoBinding: FragmentWelcomeTwoBinding

    companion object {
        fun newInstance(message: String): WelcomeTwoFragment {
            val welcomeTwoFragment = WelcomeTwoFragment()
            val bundle = Bundle(1)
            bundle.putString(StaticKeysCode.TITLE, message)
            welcomeTwoFragment.arguments = bundle
            return welcomeTwoFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentWelcomeTwoBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_welcome_two, container, false)
        return fragmentWelcomeTwoBinding.root
    }

    override fun initData(view: View) {
        val title = arguments!!.getString(StaticKeysCode.TITLE)
    }

    override fun initListener(view: View) {

    }
}