package com.instaprep.welcome

import androidx.databinding.DataBindingUtil
import com.instaprep.R
import com.instaprep.auth.LoginActivity
import com.instaprep.databinding.ActivityWelcomeBinding
import com.dorna.ui.base.BaseActivity
import com.dorna.ui.base.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : BaseActivity() {

    private lateinit var activityWelcomeBinding: ActivityWelcomeBinding
    override fun initView() {
        activityWelcomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_welcome)
    }

    private lateinit var viewPagerAdapter: ViewPagerAdapter

    override fun initData() {
        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        var welcomeOneFragment: WelcomeOneFragment =
            WelcomeOneFragment.newInstance(getString(R.string.welcome_one_fragment_title))
        var welcomeTwoFragment: WelcomeTwoFragment =
            WelcomeTwoFragment.newInstance(getString(R.string.welcome_two_fragment_title))
        var welcomeThreeFragment: WelcomeThreeFragment =
            WelcomeThreeFragment.newInstance(getString(R.string.welcome_three_fragment_title))
        viewPagerAdapter.addFragment(
            welcomeOneFragment,
            getString(R.string.welcome_one_fragment_title)
        )
        viewPagerAdapter.addFragment(
            welcomeTwoFragment,
            getString(R.string.welcome_two_fragment_title)
        )
        viewPagerAdapter.addFragment(
            welcomeThreeFragment,
            getString(R.string.welcome_three_fragment_title)
        )
        welcomeViewPager.adapter = viewPagerAdapter
        dots_indicator.setViewPager(welcomeViewPager);
    }

    override fun initListener() {
        nextButton.setOnClickListener {
            moveToNext(LoginActivity::class.java, finishCurrent = true, clearStack = true)
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }
    }
}