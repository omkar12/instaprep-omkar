package com.instaprep.welcome

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.instaprep.R
import com.instaprep.databinding.FragmentWelcomeOneBinding
import com.instaprep.utils.StaticKeysCode.Companion.TITLE
import com.dorna.ui.base.BaseFragment

class WelcomeOneFragment : BaseFragment() {
    private lateinit var fragmentWelcomeOneBinding: FragmentWelcomeOneBinding

    companion object {
        fun newInstance(message: String): WelcomeOneFragment {
            val welcomeOneFragment = WelcomeOneFragment()
            val bundle = Bundle(1)
            bundle.putString(TITLE, message)
            welcomeOneFragment.arguments = bundle
            return welcomeOneFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentWelcomeOneBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_welcome_one, container, false)
        return fragmentWelcomeOneBinding.root
    }

    override fun initData(view: View) {
        val title = arguments!!.getString(TITLE)
    }

    override fun initListener(view: View) {

    }

}