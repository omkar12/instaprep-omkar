package com.instaprep.welcome

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.instaprep.R
import com.instaprep.databinding.FragmentWelcomeThreeBinding
import com.instaprep.utils.StaticKeysCode
import com.dorna.ui.base.BaseFragment

class WelcomeThreeFragment : BaseFragment() {
    private lateinit var fragmentWelcomeThreeBinding: FragmentWelcomeThreeBinding

    companion object {
        fun newInstance(message: String): WelcomeThreeFragment {
            val welcomeThreeFragment = WelcomeThreeFragment()
            val bundle = Bundle(1)
            bundle.putString(StaticKeysCode.TITLE, message)
            welcomeThreeFragment.arguments = bundle
            return welcomeThreeFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentWelcomeThreeBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_welcome_three, container, false)
        return fragmentWelcomeThreeBinding.root
    }

    override fun initData(view: View) {
        val title = arguments!!.getString(StaticKeysCode.TITLE)
    }

    override fun initListener(view: View) {
    }

}