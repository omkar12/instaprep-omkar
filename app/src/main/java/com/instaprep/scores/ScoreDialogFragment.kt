package com.instaprep.scores

import android.animation.ValueAnimator
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import com.instaprep.R
import kotlinx.android.synthetic.main.fragment_dialog_score.view.*


class ScoreDialogFragment : DialogFragment() {

    companion object {
        var TAG: String = ""
            get() {
                return ScoreDialogFragment::class.java.simpleName
            }

        fun newInstance(): ScoreDialogFragment {
            val dashboardFragment = ScoreDialogFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_dialog_score, container, false)
        simulateProgress(view)
        view.checkAnswersText.setOnClickListener {

        }
        return view
    }

    override fun onStart() {
        super.onStart()
        val params = dialog!!.window!!.attributes/*
        val width = resources.getDimensionPixelSize(R.dimen.popup_margin_left)
        val height = resources.getDimensionPixelSize(R.dimen.popup_margin_right)*/
        params.width = ConstraintLayout.LayoutParams.MATCH_PARENT
        params.height = ConstraintLayout.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as android.view.WindowManager.LayoutParams
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
    }

    private fun simulateProgress(view: View) {
        //standard progress
        val animatorStandard = ValueAnimator.ofInt(1, 75)
        animatorStandard.addUpdateListener { animation ->
            val progress = animation.animatedValue as Int
            println("value: $progress")
            view.standardProgress.progress = progress
            view.cbaProgress.progress = 50
        }
        //  animator.repeatCount = ValueAnimator.INFINITE
        animatorStandard.duration = 800
        animatorStandard.start()
        //cba progress
        val animatorCBA = ValueAnimator.ofInt(1, 50)
        animatorCBA.addUpdateListener { animation ->
            val progress = animation.animatedValue as Int
            view.cbaProgress.progress = progress
        }
        //  animator.repeatCount = ValueAnimator.INFINITE
        animatorCBA.duration = 1000
        animatorCBA.start()


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog!!.window!!
            .attributes.windowAnimations = R.style.DialogAnimation;
    }
}