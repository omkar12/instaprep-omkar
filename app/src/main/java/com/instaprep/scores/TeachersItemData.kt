package com.instaprep.scores

data class TeachersItemData(val id: Int? = 0, val name: String? = "", val phoneNo: String? = "")