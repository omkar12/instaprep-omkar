package com.instaprep.scores

interface TeachersItemClickListener {
    fun viewTeacherProfile(teacherId: Int)
}