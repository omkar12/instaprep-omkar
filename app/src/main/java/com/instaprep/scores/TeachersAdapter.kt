package com.instaprep.scores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.instaprep.R
import kotlinx.android.synthetic.main.activity_score_teacher_item.view.*

class TeachersAdapter : RecyclerView.Adapter<TeachersAdapter.ItemViewHolder> {

    private val context: Context
    private val dataList: ArrayList<TeachersItemData>
    private val itemClickListener: TeachersItemClickListener

    constructor(
        context: Context,
        dataList: ArrayList<TeachersItemData>,
        itemClickListener: TeachersItemClickListener
    ) : super() {
        this.context = context
        this.dataList = dataList
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_score_teacher_item, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindItem(dataList[position])
        holder.itemView.setOnClickListener {
            itemClickListener.viewTeacherProfile(dataList[position].id!!)
        }
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(teachersItemData: TeachersItemData) {
            itemView.sNo.text = (adapterPosition + 1).toString()
            itemView.teachersName.text = teachersItemData.name
            itemView.teachersPhoneNo.text = teachersItemData.phoneNo
        }

    }
}