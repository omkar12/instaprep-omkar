package com.instaprep.scores

import android.animation.ValueAnimator
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.instaprep.R
import com.instaprep.databinding.ActivityScoreBinding
import com.dorna.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_score.*

class ScoreActivity : BaseActivity(), TeachersItemClickListener {

    private lateinit var activityScoreBinding: ActivityScoreBinding
    private lateinit var adapter: TeachersAdapter
    private var teachersData: ArrayList<TeachersItemData> = ArrayList()
    override fun initView() {
        activityScoreBinding = DataBindingUtil.setContentView(this, R.layout.activity_score)
    }

    override fun initData() {
        adapter = TeachersAdapter(this, teachersData, this)
        teachers.layoutManager = LinearLayoutManager(this)
        teachers.adapter = adapter

        simulateProgress()
        addData()
    }

    override fun initListener() {
    }

    override fun viewTeacherProfile(teacherId: Int) {

    }

    private fun addData() {
        if (teachersData.isNotEmpty()) teachersData.clear()

        teachersData.add(TeachersItemData(1, "Anup Raj", "+914573845376"))
        teachersData.add(TeachersItemData(1, "Anup Raj", "+914573845376"))
        teachersData.add(TeachersItemData(1, "Anup Raj", "+914573845376"))
        teachersData.add(TeachersItemData(1, "Anup Raj", "+914573845376"))
        teachersData.add(TeachersItemData(1, "Anup Raj", "+914573845376"))
        teachersData.add(TeachersItemData(1, "Anup Raj", "+914573845376"))
        teachersData.add(TeachersItemData(1, "Anup Raj", "+914573845376"))
        teachersData.add(TeachersItemData(1, "Anup Raj", "+914573845376"))

        adapter.notifyDataSetChanged()
    }

    private fun simulateProgress() {
        //standard progress
        val animatorStandard = ValueAnimator.ofInt(1, 75)
        animatorStandard.addUpdateListener { animation ->
            val progress = animation.animatedValue as Int
            println("value: $progress")
            standardProgress.progress = progress
            cbaProgress.progress = 50
        }
        //  animator.repeatCount = ValueAnimator.INFINITE
        animatorStandard.duration = 800
        animatorStandard.start()
        //cba progress
        val animatorCBA = ValueAnimator.ofInt(1, 50)
        animatorCBA.addUpdateListener { animation ->
            val progress = animation.animatedValue as Int
            cbaProgress.progress = progress
        }
        //  animator.repeatCount = ValueAnimator.INFINITE
        animatorCBA.duration = 1000
        animatorCBA.start()


    }
}