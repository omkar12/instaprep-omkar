package com.instaprep.classes

import android.animation.ObjectAnimator
import android.os.Handler
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.instaprep.R
import com.instaprep.dashboard.DashBoardActivity
import com.instaprep.databinding.ActivityClassesBinding
import com.instaprep.utils.StaticKeysCode
import com.instaprep.utils.snackBar
import com.dorna.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_classes.*

class ClassesActivity : BaseActivity(), ClassSelectionListener {

    private lateinit var activityClassesBinding: ActivityClassesBinding
    private var dataList: ArrayList<ClassItemData> = ArrayList()
    private lateinit var adapter: ClassAdapter
    private var selectedClass: Boolean = false

    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 1000 //1 seconds
    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            moveToNext(DashBoardActivity::class.java, finishCurrent = false, clearStack = false)
        }
    }

    override fun initView() {
        activityClassesBinding =
            DataBindingUtil.setContentView(this@ClassesActivity, R.layout.activity_classes)
    }

    override fun initData() {
        adapter = ClassAdapter(
            context = this,
            classList = dataList,
            itemClickListener = this
        )
        classRecyclerView.layoutManager = LinearLayoutManager(this@ClassesActivity)
        classRecyclerView.hasFixedSize()
        classRecyclerView.adapter = adapter

        mDelayHandler = Handler()

        addTempData()
    }

    private fun addTempData() {
        if (dataList.isNotEmpty()) dataList.clear()
        dataList.add(ClassItemData(1, "Class 6", false))
        dataList.add(ClassItemData(2, "Class 7", false))
        dataList.add(ClassItemData(3, "Class 8", false))
        dataList.add(ClassItemData(4, "Class 9", false))
        dataList.add(ClassItemData(5, "Class 10", false))
        dataList.add(ClassItemData(6, "Class 11", false))
        dataList.add(ClassItemData(7, "Class 12", false))
        adapter.notifyDataSetChanged()
    }

    override fun initListener() {
        submitButton.setOnClickListener {
            if (selectedClass) {
                moveToNext(DashBoardActivity::class.java, finishCurrent = true, clearStack = true)
                overridePendingTransition(R.anim.enter, R.anim.exit)
            } else rootView.snackBar(getString(R.string.warning_select_class_first))
            //   mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)
        }
    }

    override fun selectedClass(item: ClassItemData) {
        if (!selectedClass) {
            selectedClass = true
            showContinue()
        }
    }

    private fun showContinue() {
        submitButton.visibility = View.VISIBLE
        ObjectAnimator.ofFloat(
            submitButton, "translationY", StaticKeysCode.SCREEN_HEIGHT.toFloat(), 0f
        ).apply {
            duration = 1000
            start()
        }
    }
    override fun onDestroy() {
        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }
        super.onDestroy()
    }
}