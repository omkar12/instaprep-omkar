package com.instaprep.classes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.instaprep.R
import kotlinx.android.synthetic.main.activity_board_item.view.cardView
import kotlinx.android.synthetic.main.activity_class_item.view.*

class ClassAdapter : RecyclerView.Adapter<ClassAdapter.BoardItemViewHolder> {

    private val context: Context
    private val classList: ArrayList<ClassItemData>
    private val itemClickListener: ClassSelectionListener
    private var lastSelectedPosition = 0

    constructor(
        context: Context,
        classList: ArrayList<ClassItemData>,
        itemClickListener: ClassSelectionListener
    ) : super() {
        this.context = context
        this.classList = classList
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoardItemViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.activity_class_item, parent, false)
        return BoardItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return classList.size
    }

    override fun onBindViewHolder(holder: BoardItemViewHolder, position: Int) {
        holder.bindItem(context, classList[position])

        holder.itemView.setOnClickListener {
            classList[position].selected = true
            notifyItemChanged(position)
            if (lastSelectedPosition != position) {
                classList[lastSelectedPosition].selected = false
                notifyItemChanged(lastSelectedPosition)
                lastSelectedPosition = position
            }
            itemClickListener.selectedClass(classList[position])
        }
    }

    class BoardItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(context: Context, classItemData: ClassItemData) {
            itemView.classNameText.text = classItemData.className
            if (classItemData.selected)
                itemView.cardView.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorBgBoardSelected
                    )
                )
            else itemView.cardView.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.colorWhite
                )
            )
        }

    }
}