package com.instaprep.classes

interface ClassSelectionListener {

    fun selectedClass(item: ClassItemData)
}