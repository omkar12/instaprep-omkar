package com.instaprep.classes

data class ClassItemData(val id: Int? = 0, val className: String? = "", var selected:Boolean)