package com.instaprep.dashboard.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.dorna.ui.base.BaseFragment
import com.instaprep.R
import com.instaprep.databinding.FragmentProfileBinding
import kotlinx.android.synthetic.main.fragment_profile.view.*

class ProfileFragment : BaseFragment() {
    private lateinit var fragmentProfileBinding: FragmentProfileBinding

    companion object {
        var TAG: String = ""
            get() {
                return ProfileFragment::class.java.simpleName
            }

        fun newInstance(): ProfileFragment {
            val dashboardFragment = ProfileFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentProfileBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        return fragmentProfileBinding.root
    }

    override fun initData(view: View) {
    }

    override fun initListener(view: View) {
        view.FirstName.setOnClickListener {
            baseActivityListener.addFragment(
                NameFragment.newInstance(), NameFragment.TAG,
                addReplace = false,
                addToBackStack = true
            )

        }
        view.LastName.setOnClickListener {
            baseActivityListener.addFragment(
                LastNameFragment.newInstance(), LastNameFragment.TAG,
                addReplace = false,
                addToBackStack = true
            )

        }
        view.EmailId.setOnClickListener {
            baseActivityListener.addFragment(
                MailFragment.newInstance(), MailFragment.TAG,
                addReplace = false,
                addToBackStack = true
            )

        }
        view.Phone.setOnClickListener {
            baseActivityListener.addFragment(
                PhoneFragment.newInstance(), PhoneFragment.TAG,
                addReplace = false,
                addToBackStack = true
            )

        }
        view.PasswordId.setOnClickListener {
            baseActivityListener.addFragment(
                PasswordFragment.newInstance(), PasswordFragment.TAG,
                addReplace = false,
                addToBackStack = true
            )

        }









    }


}