package com.instaprep.dashboard.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.dorna.ui.base.BaseFragment
import com.instaprep.R
import com.instaprep.databinding.FragmentDashboardSettingsBinding
import kotlinx.android.synthetic.main.fragment_dashboard_settings.view.*

class SettingsFragment : BaseFragment() {

    private lateinit var settingsFragmentBinding: FragmentDashboardSettingsBinding

    companion object {
        var TAG: String = ""
            get() {
                return SettingsFragment::class.java.simpleName
            }

        fun newInstance(): SettingsFragment {
            val dashboardFragment = SettingsFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        settingsFragmentBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_dashboard_settings,
            container,
            false
        )
        return settingsFragmentBinding.root
    }

    override fun initData(view: View) {
    }

    override fun initListener(view: View) {

        view.share.setOnClickListener {
            baseActivityListener.addFragment(
                ShareFragment.newInstance(), ShareFragment.TAG,
                addReplace = false,
                addToBackStack = true
            )

        }
        view.myAccount.setOnClickListener {
            baseActivityListener.addFragment(
                ProfileFragment.newInstance(), ProfileFragment.TAG,
                addReplace = false,
                addToBackStack = true
            )
        }


    }
}


