package com.instaprep.dashboard.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.dorna.ui.base.BaseFragment
import com.instaprep.R
import com.instaprep.databinding.FragmentPasswordBinding

class PasswordFragment : BaseFragment()
{
    private lateinit var fragmentPasswordBinding: FragmentPasswordBinding
    companion object {
        var TAG: String = ""
            get() {
                return PasswordFragment::class.java.simpleName
            }

        fun newInstance(): PasswordFragment {
            val dashboardFragment = PasswordFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentPasswordBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_password, container, false)
        return fragmentPasswordBinding.root
    }

    override fun initData(view: View) {

    }

    override fun initListener(view: View) {

    }


}