package com.instaprep.dashboard.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.dorna.ui.base.BaseFragment
import com.instaprep.R
import com.instaprep.databinding.FragmentNameBinding

class NameFragment : BaseFragment()
{
    private lateinit var fragmentNameBinding: FragmentNameBinding
    companion object {
        var TAG: String = ""
            get() {
                return NameFragment::class.java.simpleName
            }

        fun newInstance(): NameFragment {
            val dashboardFragment = NameFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentNameBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_name, container, false)
        return fragmentNameBinding.root
    }

    override fun initData(view: View) {

    }

    override fun initListener(view: View) {

    }


}