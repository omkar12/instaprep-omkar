package com.instaprep.dashboard.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.dorna.ui.base.BaseFragment
import com.instaprep.R
import com.instaprep.databinding.FragmentMailBinding

class MailFragment : BaseFragment()
{
    private lateinit var fragmentMailBinding: FragmentMailBinding
    companion object {
        var TAG: String = ""
            get() {
                return MailFragment::class.java.simpleName
            }

        fun newInstance(): MailFragment {
            val dashboardFragment = MailFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentMailBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_mail, container, false)
        return fragmentMailBinding.root
    }

    override fun initData(view: View) {

    }

    override fun initListener(view: View) {

    }


}