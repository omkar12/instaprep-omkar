package com.instaprep.dashboard.settings

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.dorna.ui.base.BaseFragment
import com.instaprep.R
import com.instaprep.databinding.FragmentShareBinding
import kotlinx.android.synthetic.main.fragment_share.view.*

class ShareFragment : BaseFragment() {
    private lateinit var fragmentShareBinding: FragmentShareBinding

    companion object {
        var TAG: String = ""
            get() {
                return ShareFragment::class.java.simpleName
            }

        fun newInstance(): ShareFragment {
            val dashboardFragment = ShareFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentShareBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_share, container, false)
        return fragmentShareBinding.root
    }

    override fun initData(view: View) {
    }

    override fun initListener(view: View) {

        view.shareWhatsapp.setOnClickListener {
            val s = view.inviteCode.text.toString()
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.setPackage("com.whatsapp")
            shareIntent.putExtra(Intent.EXTRA_TEXT, s)
            shareIntent.putExtra(Intent.EXTRA_SUBJECT,  "Subject here")

            startActivity(Intent.createChooser(shareIntent, "Share text via.."))

        }
        view.shareVia.setOnClickListener {
            val s = view.inviteCode.text.toString()
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"

            shareIntent.putExtra(Intent.EXTRA_TEXT, s)
            shareIntent.putExtra(Intent.EXTRA_SUBJECT,  "Subject here")

            startActivity(Intent.createChooser(shareIntent, "Share text via.."))

        }


    }



}
