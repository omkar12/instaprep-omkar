package com.instaprep.dashboard.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.dorna.ui.base.BaseFragment
import com.instaprep.R
import com.instaprep.databinding.FragmentPhoneBinding

class PhoneFragment : BaseFragment()
{
    private lateinit var fragmentPhoneBinding: FragmentPhoneBinding
    companion object {
        var TAG: String = ""
            get() {
                return PhoneFragment::class.java.simpleName
            }

        fun newInstance(): PhoneFragment {
            val dashboardFragment = PhoneFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentPhoneBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_phone, container, false)
        return fragmentPhoneBinding.root
    }

    override fun initData(view: View) {

    }

    override fun initListener(view: View) {

    }


}