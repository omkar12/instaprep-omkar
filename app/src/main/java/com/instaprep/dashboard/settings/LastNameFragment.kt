package com.instaprep.dashboard.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.dorna.ui.base.BaseFragment
import com.instaprep.R
import com.instaprep.databinding.FragmentLastNameBinding

class LastNameFragment : BaseFragment()
{
    private lateinit var fragmentLastNameBinding: FragmentLastNameBinding
    companion object {
        var TAG: String = ""
            get() {
                return LastNameFragment::class.java.simpleName
            }

        fun newInstance(): LastNameFragment {
            val dashboardFragment = LastNameFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentLastNameBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_last_name, container, false)
        return fragmentLastNameBinding.root
    }

    override fun initData(view: View) {

    }

    override fun initListener(view: View) {

    }


}