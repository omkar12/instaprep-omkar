package com.instaprep.dashboard.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.instaprep.R
import com.instaprep.databinding.FragmentDashboardAccountBinding
import com.dorna.ui.base.BaseFragment

class AccountFragment : BaseFragment() {
    private lateinit var fragmentDashboardAccountBinding: FragmentDashboardAccountBinding

    companion object {
        var TAG: String = ""
            get() {
                return AccountFragment::class.java.simpleName
            }

        fun newInstance(): AccountFragment {
            val dashboardFragment = AccountFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentDashboardAccountBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard_account, container, false)
        return fragmentDashboardAccountBinding.root
    }

    override fun initData(view: View) {
    }

    override fun initListener(view: View) {
    }


}