package com.instaprep.dashboard.todo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.instaprep.R

class ToDoAdapter : RecyclerView.Adapter<ToDoAdapter.ItemViewHolder> {

    private var dataList: ArrayList<ToDoItem>
    private var itemSelectionListener: ToDoItemSelectionListener

    constructor(dataList: ArrayList<ToDoItem>, itemSelectionListener: ToDoItemSelectionListener) : super() {
        this.dataList = dataList
        this.itemSelectionListener = itemSelectionListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_dashboard_to_do_item, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindItem(dataList[position])
        holder.itemView.setOnClickListener {
            itemSelectionListener.selectedToDoItem(position)
        }
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(toDoItem: ToDoItem) {
            val itemName = itemView.findViewById<TextView>(R.id.itemName)
            itemName.text = toDoItem.name
        }

    }
}