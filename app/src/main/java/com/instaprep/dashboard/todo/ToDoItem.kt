package com.instaprep.dashboard.todo

data class ToDoItem(val id:Int, val name:String)