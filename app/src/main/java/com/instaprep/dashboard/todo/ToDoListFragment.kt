package com.instaprep.dashboard.todo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.instaprep.R
import com.instaprep.databinding.FragmentDashboardToDoListBinding
import com.dorna.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_dashboard_to_do_list.view.*

class ToDoListFragment : BaseFragment(), ToDoItemSelectionListener {
    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        fragmentDashboardToDoListBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_dashboard_to_do_list,
            container,
            false
        )
        return fragmentDashboardToDoListBinding.root
    }

    override fun initListener(view: View) {
    }

    private lateinit var fragmentDashboardToDoListBinding: FragmentDashboardToDoListBinding
    private lateinit var toDoAdapter: ToDoAdapter
    private lateinit var toDoList: ArrayList<ToDoItem>

    companion object {
        var TAG: String = ""
            get() {
                return ToDoListFragment::class.java.simpleName
            }

        fun newInstance(): ToDoListFragment {
            val dashboardFragment = ToDoListFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initData(view: View) {
        toDoList = ArrayList()

        toDoAdapter =
            ToDoAdapter(dataList = toDoList, itemSelectionListener = this)
        view.toDoRecyclerView.layoutManager = LinearLayoutManager(context)
        view.toDoRecyclerView.adapter = toDoAdapter

        addData()

    }

    private fun addData() {
        toDoList.add(ToDoItem(1, "Basic Math Test"))
        toDoList.add(ToDoItem(2, "Live Test"))
        toDoList.add(ToDoItem(3, "Olympiad Test"))

        toDoAdapter.notifyDataSetChanged()
    }

    override fun selectedToDoItem(position: Int) {

    }
}