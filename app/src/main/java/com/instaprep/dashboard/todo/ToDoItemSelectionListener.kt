package com.instaprep.dashboard.todo

interface ToDoItemSelectionListener {

    fun selectedToDoItem(position:Int)
}