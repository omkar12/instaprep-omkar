package com.instaprep.dashboard.home.subjects

interface SubjectItemSelectionListener {

    fun selectedSubject(position: Int)
}