package com.instaprep.dashboard.home.subjects

data class SubjectItems(val id:Int, val name:String)