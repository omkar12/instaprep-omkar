package com.instaprep.dashboard.home.upcoming

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.instaprep.R
import com.instaprep.databinding.FragmentHomeUpcomingTestItemBinding
import com.instaprep.utils.StaticKeysCode.Companion.DATE_TIME
import com.instaprep.utils.StaticKeysCode.Companion.ID
import com.instaprep.utils.StaticKeysCode.Companion.TITLE
import com.dorna.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_home_upcoming_test_item.view.*

class HomeUpComingFragment : BaseFragment() {
    private lateinit var dashboardUpcomingTestItemBinding: FragmentHomeUpcomingTestItemBinding
    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        dashboardUpcomingTestItemBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home_upcoming_test_item,
            container,
            false
        )
        return dashboardUpcomingTestItemBinding.root
    }

    override fun initListener(view: View) {
    }

    companion object {
        fun newInstance(id: Int, title: String, dateTime: String): HomeUpComingFragment {
            val dashboardFragment = HomeUpComingFragment()
            val bundle = Bundle(3)
            bundle.putInt(ID, id)
            bundle.putString(TITLE, title)
            bundle.putString(DATE_TIME, dateTime)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initData(view: View) {
        val bundle = arguments
        val id = bundle!!.getInt(ID)
        view.titleText.text = bundle!!.getString(TITLE)
        view.dateText.text = bundle!!.getString(DATE_TIME)
    }

}