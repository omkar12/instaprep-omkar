package com.instaprep.dashboard.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.instaprep.R
import com.instaprep.dashboard.home.subjects.SubjectAdapter
import com.instaprep.dashboard.home.subjects.SubjectItemSelectionListener
import com.instaprep.dashboard.home.subjects.SubjectItems
import com.instaprep.dashboard.home.upcoming.HomeUpComingFragment
import com.instaprep.databinding.FragmentHomeBinding
import com.dorna.ui.base.BaseFragment
import com.dorna.ui.base.StateViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : BaseFragment(),
    SubjectItemSelectionListener {

    private lateinit var dashboardHomeBinding: FragmentHomeBinding
    private lateinit var viewPagerAdapter: StateViewPagerAdapter

    //subjects
    private lateinit var subjectAdapter: SubjectAdapter
    private lateinit var subjectsData: ArrayList<SubjectItems>

    companion object {
        var TAG: String = ""
            get() {
                return HomeFragment::class.java.simpleName
            }

        fun newInstance(): HomeFragment {
            val dashboardFragment = HomeFragment()
            val bundle = Bundle(1)
            dashboardFragment.arguments = bundle
            return dashboardFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        baseActivityListener.toolbarPrimaryTitle(getString(R.string.dummy_student_name))
        baseActivityListener.toolbarSecondaryTitle(getString(R.string.dummy_student_class))
        dashboardHomeBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return dashboardHomeBinding.root
    }

    override fun initListener(view: View) {
    }

    override fun initData(view: View) {
        viewPagerAdapter = StateViewPagerAdapter(fragmentManager!!)
        viewPagerAdapter.addFragment(
            HomeUpComingFragment.newInstance(
                1,
                "Math Olympiad",
                "12 August 2019"
            ),
            getString(R.string.label_upcoming_tests)
        )
        viewPagerAdapter.addFragment(
            HomeUpComingFragment.newInstance(
                2,
                "Physics Olympiad",
                "20 August 2019"
            ),
            getString(R.string.label_upcoming_tests)
        )
        viewPagerAdapter.addFragment(
            HomeUpComingFragment.newInstance(
                3,
                "Chemistry Olympiad",
                "5 September 2019"
            ),
            getString(R.string.label_upcoming_tests)
        )

        view.upcomingViewPager.adapter = viewPagerAdapter
        // Disable clip to padding
        view.upcomingViewPager.clipToPadding = false;
        // set padding manually, the more you set the padding the more you see of prev & next page
        view.upcomingViewPager.setPadding(120, 0, 120, 0);
        // sets a margin b/w individual pages to ensure that there is a gap b/w them
        view.upcomingViewPager.pageMargin = 30;

        //subjects
        subjectsData = ArrayList()
        subjectAdapter = SubjectAdapter(
            dataList = subjectsData,
            itemSelectionListener = this
        )
        view.subjectsRecyclerView.layoutManager = GridLayoutManager(context, 3)
        view.subjectsRecyclerView.hasFixedSize()
        view.subjectsRecyclerView.adapter = subjectAdapter

        addSubjects()
    }

    private fun addSubjects() {

        if (subjectsData.isNotEmpty()) subjectsData.clear()
        subjectsData.add(SubjectItems(1, "Chemistry"))
        subjectsData.add(SubjectItems(2, "Physics"))
        subjectsData.add(SubjectItems(3, "Maths"))
        subjectsData.add(SubjectItems(4, "English"))
        subjectsData.add(SubjectItems(5, "Computer"))
        subjectsData.add(SubjectItems(6, "Hindi"))

        subjectAdapter.notifyDataSetChanged()
    }

    override fun selectedSubject(position: Int) {
    }
}