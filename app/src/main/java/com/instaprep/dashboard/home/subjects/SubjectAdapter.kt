package com.instaprep.dashboard.home.subjects

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.instaprep.R

class SubjectAdapter : RecyclerView.Adapter<SubjectAdapter.ItemViewHolder> {

    private var dataList: ArrayList<SubjectItems>
    private var itemSelectionListener: SubjectItemSelectionListener

    constructor(dataList: ArrayList<SubjectItems>, itemSelectionListener: SubjectItemSelectionListener) : super() {
        this.dataList = dataList
        this.itemSelectionListener = itemSelectionListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_dashboard_subject_item, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindItem(dataList[position])
        holder.itemView.setOnClickListener {
            itemSelectionListener.selectedSubject(position)
        }
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(subjectItems: SubjectItems) {
            val subjectName = itemView.findViewById<TextView>(R.id.subjectName)
            subjectName.text = subjectItems.name
        }
    }
}