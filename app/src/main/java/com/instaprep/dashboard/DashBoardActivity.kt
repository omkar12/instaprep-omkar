package com.instaprep.dashboard

import android.util.Log
import androidx.databinding.DataBindingUtil
import com.instaprep.R
import com.instaprep.databinding.ActivityDashboardBinding
import com.instaprep.tests.StartTestFragment
import com.dorna.ui.base.BaseActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.instaprep.dashboard.home.HomeFragment
import com.instaprep.dashboard.settings.ProfileFragment
import com.instaprep.dashboard.settings.SettingsFragment
import com.katariya.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_dashboard.*


class DashBoardActivity : BaseActivity(), BottomNavigationView.BottomMenuListener {

    private lateinit var dashboardBinding: ActivityDashboardBinding
    override fun initView() {
        dashboardBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
    }

    override fun initData() {

        FirebaseMessaging.getInstance().isAutoInitEnabled = true

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("TAG", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                //val token = task.result?.token

                // Log and toast
                //   Toast.makeText(baseContext, token, Toast.LENGTH_SHORT).show()
            })



        bottomNavigationView.addChild(
            R.drawable.ic_home_inactive_icon,
            R.drawable.ic_home_active_icon,
            null
        )
        bottomNavigationView.addChild(
            R.drawable.ic_to_do_list_inactive_icon,
            R.drawable.ic_to_do_list_active_icon,
            null
        )
        bottomNavigationView.addChild(
            R.drawable.ic_profile_inactive_icon,
            R.drawable.ic_profile_active_icon,
            null
        )
        bottomNavigationView.addChild(
            R.drawable.ic_settings_inactive_icon,
            R.drawable.ic_settings_active_icon,
            null
       )

        addFragment(
            HomeFragment.newInstance(),
            HomeFragment.TAG,
            addReplace = true,
            addToBackStack = false
        )

    }

    override fun initListener() {
        bottomNavigationView.initItemListener(this)
              bottomNavigationView.setSelected(0)
    }

    override fun bottomNavigationViewItemClickListener(position: Int) {
        when (position) {
            0 -> /* val fragmentDashboard = supportFragmentManager.findFragmentByTag(HomeFragment.TAG)
                if (fragmentDashboard == null) {

                } else {
                    //fragment exist
                }*/
                addFragment(
                    HomeFragment.newInstance(),
                    HomeFragment.TAG,
                    addReplace = false,
                    addToBackStack = false
                )
            1 -> addFragment(
                StartTestFragment.newInstance(),
                StartTestFragment.TAG,
                addReplace = false,
                addToBackStack = false
            )
            2 -> addFragment(
                ProfileFragment.newInstance(),
                ProfileFragment.TAG,
                addReplace = false,
                addToBackStack = false
            )
            3 -> addFragment(
                SettingsFragment.newInstance(),
                SettingsFragment.TAG,
                addReplace = false,
                addToBackStack = false
            )
        }
    }
    override fun toolbarPrimaryTitle(title: String) {
        super.toolbarPrimaryTitle(title)
        toolbarPrimaryText.text = title
    }

    override fun toolbarSecondaryTitle(title: String) {
        super.toolbarSecondaryTitle(title)
        toolbarSecondaryText.text = title
    }

}
