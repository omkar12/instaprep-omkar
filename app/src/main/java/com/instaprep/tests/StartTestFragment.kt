package com.instaprep.tests

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.instaprep.R
import com.instaprep.databinding.FragmentStartTestBinding
import com.dorna.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_start_test.view.*

class StartTestFragment : BaseFragment() {

    private lateinit var fragmentStartTestBinding: FragmentStartTestBinding

    companion object {
        var TAG: String = ""
            get() {
                return StartTestFragment::class.java.simpleName
            }

        fun newInstance(): StartTestFragment {
            val startTestFragment = StartTestFragment()
            val bundle = Bundle(1)
            startTestFragment.arguments = bundle
            return startTestFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        baseActivityListener.toolbarPrimaryTitle(getString(R.string.title_basic_math_test))
        baseActivityListener.toolbarSecondaryTitle(getString(R.string.label_all_the_best))
        fragmentStartTestBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_start_test, container, false)
        return fragmentStartTestBinding.root
    }

    override fun initData(view: View) {

    }

    override fun initListener(view: View) {
        view.startTestButton.setOnClickListener {
            baseActivityListener.addFragment(
                QuestionFragment.newInstance(), QuestionFragment.TAG,
                addReplace = false,
                addToBackStack = true
            )
        }
    }

}