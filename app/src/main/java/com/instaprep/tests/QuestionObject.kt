package com.instaprep.tests

data class QuestionObject(val id: Int, val title: String, val image: String, val answerArrayList: ArrayList<String>) {}