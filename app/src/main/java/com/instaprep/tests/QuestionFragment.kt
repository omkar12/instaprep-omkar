package com.instaprep.tests

import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.instaprep.R
import com.instaprep.databinding.FragmentQuestionBinding
import com.instaprep.scores.ScoreActivity
import com.instaprep.utils.slide
import com.dinuscxj.progressbar.CircleProgressBar
import com.dorna.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_question.view.*
import kotlinx.android.synthetic.main.layout_question_answer_confirm_view.view.*
import kotlinx.android.synthetic.main.layout_question_view.*
import kotlinx.android.synthetic.main.layout_question_view.view.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class QuestionFragment : BaseFragment() {

    private var questionsList: ArrayList<QuestionObject> = ArrayList()

    private lateinit var fragmentQuestionBinding: FragmentQuestionBinding
    private val handler: Handler = Handler()
    private lateinit var runnable: Runnable
    private lateinit var testTimer: CountDownTimer
    lateinit var timerBar: CircleProgressBar
    private var percentCount = 0
    private var timeCount = 0


    companion object {
        var TAG: String = ""
            get() {
                return QuestionFragment::class.java.simpleName
            }

        fun newInstance(): QuestionFragment {
            val questionFragment = QuestionFragment()
            val bundle = Bundle(1)
            questionFragment.arguments = bundle
            return questionFragment
        }
    }

    override fun initView(inflater: LayoutInflater, container: ViewGroup?): View {
        baseActivityListener.toolbarPrimaryTitle(getString(R.string.title_basic_math_test))
        baseActivityListener.toolbarSecondaryTitle(getString(R.string.label_all_the_best))
        fragmentQuestionBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_question, container, false)

        return fragmentQuestionBinding.root
    }

    override fun initData(view: View) {

        var answerArray1: ArrayList<String> = ArrayList()

        answerArray1.add("2")
        answerArray1.add("3")
        answerArray1.add("4")
        answerArray1.add("5")


        var answerArray2: ArrayList<String> = ArrayList()

        answerArray2.add("37600")
        answerArray2.add("37500")
        answerArray2.add("37700")
        answerArray2.add("37800")


        var answerArray3: ArrayList<String> = ArrayList()

        answerArray3.add("60 km")
        answerArray3.add("50 km")
        answerArray3.add("40 km")
        answerArray3.add("70 km")


        var answerArray4: ArrayList<String> = ArrayList()

        answerArray4.add("40 hrs")
        answerArray4.add("30 hrs")
        answerArray4.add("50 hrs")
        answerArray4.add("60 hrs")


        var answerArray5: ArrayList<String> = ArrayList()

        answerArray5.add("55")
        answerArray5.add("65")
        answerArray5.add("45")
        answerArray5.add("75")


        var question1: QuestionObject = QuestionObject(
            1,
            "What is the least number that should be subtracted from 13442 to make itexactly divisible by 5?",
            "", answerArray1
        )


        var question2: QuestionObject = QuestionObject(
            2,
            "75 x 500 = _______?",
            "", answerArray2
        )

        var question3: QuestionObject = QuestionObject(
            3,
            "A bus covers 200km in 4 hours . How much distance does it cover in 1 hour?",
            "", answerArray3
        )


        var question4: QuestionObject = QuestionObject(
            4,
            "How many hours are there in 300 minutes?",
            "", answerArray4
        )

        var question5: QuestionObject = QuestionObject(
            5,
            "The 5 th multiple of 9 is_",
            "", answerArray5
        )



        questionsList.add(question1)
        questionsList.add(question2)
        questionsList.add(question3)
        questionsList.add(question4)
        questionsList.add(question5)


        view.questionText1.setText(questionsList.get(0).title);
        view.questionNoText1.setText(questionsList.get(0).id.toString() + ")")
        view.optionOneRadioButton1.setText(questionsList.get(0).answerArrayList.get(0))
        view.optionTwoRadioButton1.setText(questionsList.get(0).answerArrayList.get(1))
        view.optionThreeRadioButton1.setText(questionsList.get(0).answerArrayList.get(2))
        view.optionFourRadioButton.setText(questionsList.get(0).answerArrayList.get(3))

        questionsList.removeAt(0)

        System.out.println("New list size==" + questionsList.size)

        val itr = questionsList.iterator()

        //move to confident view

        runnable = Runnable {

            view.confidentView.slide(view.questionAnswerView, 500, View.VISIBLE, Gravity.RIGHT)

            if (itr.hasNext()) {

                var newQuestion: QuestionObject = itr.next()

                questionText.setText(newQuestion.title);
                questionNoText.setText(newQuestion.id.toString() + ")")
                optionOneRadioButton.setText(newQuestion.answerArrayList.get(0))
                optionTwoRadioButton.setText(newQuestion.answerArrayList.get(1))
                optionThreeRadioButton.setText(newQuestion.answerArrayList.get(2))
                optionFourRadioButton.setText(newQuestion.answerArrayList.get(3))

            }


        }

        startTimer(view)

    }

    private fun startTimer(view: View) {
        //init progress bar
        timerBar = view.circularProgressbar

        testTimer = object : CountDownTimer(5 * 60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

                if (timeCount == 5) {

                    timeCount = 0
                    percentCount++

                }

                timeCount++

                println("$percentCount")
                println("$timeCount")

                view.circularProgressbar.progress = percentCount

                val text = String.format(
                    Locale.getDefault(), "%02d min: %02d sec",
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60
                )
                view.timeText.text = text
            }

            override fun onFinish() {

            }
        }
        testTimer.start()

    }

    private fun addTempQuestions() {
        if (questionsList.isNotEmpty()) questionsList.clear()

        var answerArray1: ArrayList<String> = ArrayList()

        answerArray1.add("2")
        answerArray1.add("3")
        answerArray1.add("4")
        answerArray1.add("5")


        var answerArray2: ArrayList<String> = ArrayList()

        answerArray2.add("37600")
        answerArray2.add("37500")
        answerArray2.add("37700")
        answerArray2.add("37800")


        var answerArray3: ArrayList<String> = ArrayList()

        answerArray3.add("60 km")
        answerArray3.add("50 km")
        answerArray3.add("40 km")
        answerArray3.add("70 km")


        var answerArray4: ArrayList<String> = ArrayList()

        answerArray4.add("40 hrs")
        answerArray4.add("30 hrs")
        answerArray4.add("50 hrs")
        answerArray4.add("60 hrs")


        var answerArray5: ArrayList<String> = ArrayList()

        answerArray5.add("55")
        answerArray5.add("65")
        answerArray5.add("45")
        answerArray5.add("75")


        var question1: QuestionObject = QuestionObject(
            1,
            "What is the least number that should be subtracted from 13442 to make itexactly divisible by 5?",
            "", answerArray1
        )


        var question2: QuestionObject = QuestionObject(
            2,
            "75 x 500 = _______?",
            "", answerArray2
        )

        var question3: QuestionObject = QuestionObject(
            3,
            "A bus covers 200km in 4 hours . How much distance does it cover in 1 hour?",
            "", answerArray3
        )


        var question4: QuestionObject = QuestionObject(
            4,
            "How many hours are there in 300 minutes?",
            "", answerArray4
        )

        var question5: QuestionObject = QuestionObject(
            5,
            "The 5 th multiple of 9 is_",
            "", answerArray5
        )



        questionsList.add(question1)
        questionsList.add(question2)
        questionsList.add(question3)
        questionsList.add(question4)
        questionsList.add(question5)

    }

    override fun initListener(view: View) {


        view.nextButton.setOnClickListener {

            view.questionView.slide(view.questionAnswerView, 300, View.GONE, Gravity.LEFT)

            handler.postDelayed(runnable, 300)

        }


        var temp: Int = -1

        val itr = questionsList.iterator()

        System.out.println("On Function list size==" + questionsList.size)


        view.submitButton.setOnClickListener {

            if (itr.hasNext()) {

                temp++

                System.out.println("On has Next ==" + questionsList.size)

                System.out.println("On has Next TEMP VALUE ==" + temp)

                if (questionsList.size == temp) {

                    handler.postDelayed(runnable, 300)

                    baseActivityListener.moveToNext(
                        ScoreActivity::class.java,
                        finishCurrent = false,
                        clearStack = false

                    )


                } else {

                    view.confidentView.slide(view.confidentView, 500, View.GONE, Gravity.RIGHT)
                    view.questionView.slide(view.questionAnswerView, 300, View.VISIBLE, Gravity.RIGHT)

                }


            } else {

                System.out.println("Has Not Next==" + questionsList.size)


                handler.postDelayed(runnable, 300)
//
//                    /* val scoreDialogFragment = ScoreDialogFragment.newInstance()
//                     scoreDialogFragment.show(fragmentManager!!, ScoreDialogFragment.TAG)*/
//
                baseActivityListener.moveToNext(
                    ScoreActivity::class.java,
                    finishCurrent = false,
                    clearStack = false

                )


            }


        }
    }

    override fun onDestroyView() {
        if (runnable != null)
            handler.removeCallbacks(runnable)
        if (testTimer != null)
            testTimer.cancel()
        super.onDestroyView()
    }


}