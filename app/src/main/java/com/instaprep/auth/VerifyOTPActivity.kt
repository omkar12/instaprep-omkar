package com.instaprep.auth

import android.animation.ObjectAnimator
import android.os.Handler
import androidx.databinding.DataBindingUtil
import com.instaprep.R
import com.instaprep.board.BoardsActivity
import com.instaprep.databinding.ActivityOtpBinding
import com.instaprep.utils.StaticKeysCode
import com.instaprep.utils.StaticKeysCode.Companion.PHONE_NUMBER
import com.dorna.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_otp.*

class VerifyOTPActivity : BaseActivity() {
    private lateinit var verifyOtpBinding: ActivityOtpBinding

    override fun initView() {
        verifyOtpBinding = DataBindingUtil.setContentView(this, R.layout.activity_otp)
    }

    private val handler: Handler = Handler()
    private lateinit var runnable1: Runnable
    private lateinit var runnable2: Runnable

    override fun initData() {
        ObjectAnimator.ofFloat(
            otpView, "translationX", StaticKeysCode.SCREEN_WIDTH.toFloat(), 0f
        ).apply {
            duration = 1000
            start()
        }

        //get bundle data
        val bundle = intent.extras
        bundle?.let {
            phoneNo.text = bundle.getString(PHONE_NUMBER)
        }

        //fill otp info
        runnable1 = Runnable {

            otpInput.setText("${((Math.random() * 9000) + 1000).toInt()}")
            otpInput.setSelection(otpInput.text.toString().length);
        }

        //move to next screen
        runnable2 = Runnable {
            moveToNext(BoardsActivity::class.java, finishCurrent = true, clearStack = true)
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }
        handler.postDelayed(runnable1, 2000)
           handler.postDelayed(runnable2, 4000)
    }

    override fun onDestroy() {
        if (runnable1 != null)
            handler.removeCallbacks(runnable1)
        if (runnable2 != null)
            handler.removeCallbacks(runnable2)
        super.onDestroy()
    }

    override fun initListener() {
        imageButton.setOnClickListener {
            onBackPressed()
        }
    }

}