package com.instaprep.auth

import android.animation.ObjectAnimator
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.instaprep.R
import com.instaprep.databinding.ActivityLoginBinding
import com.instaprep.utils.StaticKeysCode
import com.instaprep.utils.StaticKeysCode.Companion.PHONE_NUMBER
import com.dorna.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : BaseActivity() {

    private lateinit var activityLoginBinding: ActivityLoginBinding

    override fun initView() {
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
    }

    private var aggreed: Boolean = false

    override fun initData() {
        //   phoneNoLayout.slide(rootView, 1500,  View.VISIBLE,Gravity.BOTTOM)

        println(StaticKeysCode.SCREEN_WIDTH)

        ObjectAnimator.ofFloat(
            phoneNoLayout, "translationX", StaticKeysCode.SCREEN_WIDTH.toFloat(), 0f
        ).apply {
            duration = 1000
            start()
        }

    }

    override fun initListener() {
        checkBoxView.setOnClickListener {
            if (aggreed) {
                aggreedCheckBox.isChecked = false
                aggreed = false
                enableContinueToOTP(false)
            } else {
                aggreedCheckBox.isChecked = true
                aggreed = true
                enableContinueToOTP(true)
            }
        }
        aggreedCheckBox.setOnCheckedChangeListener { buttonView, isChecked ->
            aggreed = isChecked
            enableContinueToOTP(isChecked)
        }
        phoneNumberInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                checkLength(s.toString().trim())
            }
        })

        getOTPButton.setOnClickListener {
            val edtPhone = phoneNumberInput.text.toString().trim()
            if (edtPhone.length == 10 && aggreed) {
                val bundle = Bundle()
                bundle.putString(PHONE_NUMBER, edtPhone)
                moveToNext(
                    activityName = VerifyOTPActivity::class.java,
                    bundle = bundle,
                    finishCurrent = true,
                    clearStack = true
                )
                overridePendingTransition(R.anim.enter, R.anim.exit)
            } else {
                Toast.makeText(this, getString(R.string.message_agreed), Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    private fun checkLength(chars: String) {
        if (chars.length == 10) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(contentView.windowToken, 0)
            constraintLayout.visibility = View.VISIBLE
            ObjectAnimator.ofFloat(
                constraintLayout, "translationY", StaticKeysCode.SCREEN_HEIGHT.toFloat(), 0f
            ).apply {
                duration = 1000
                start()
            }

        } else {
            ObjectAnimator.ofFloat(
                constraintLayout, "translationY", 0f, StaticKeysCode.SCREEN_HEIGHT.toFloat()
            ).apply {
                duration = 1000
                start()
            }
        }
    }
    private fun enableContinueToOTP(active:Boolean){
        if (active) {
		
            getOTPButton.setBackgroundResource(R.drawable.bg_login_get_otp_active)
            getOTPButton.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
        
		} else {
            getOTPButton.setBackgroundResource(R.drawable.bg_login_get_otp_inactive)
            getOTPButton.setTextColor(ContextCompat.getColor(this, R.color.colorBlack))
        }
    }

}